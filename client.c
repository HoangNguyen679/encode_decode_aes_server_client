#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#define BUFF_SIZE  2048

int main(int argc, char* argv[]){
	//kiem tra tham so
	int serv_port = 0;
	char serv_ip[16] = "";
	if (argc == 3) {
		struct sockaddr_in tmp_addr;
		if (inet_pton(AF_INET, argv[1], &(tmp_addr.sin_addr)) == 0) {
			printf("IP Address is invalid\n!");
			return -1;
		} else 
			strcpy(serv_ip, argv[1]);
		
		serv_port = atoi(argv[2]);
		if(serv_port < 0)
			return -1;
	} else {
		printf("Sai cu phap: ./client ip port\n");
		return -1;
	}
	
	int client_sock;
	char buff[BUFF_SIZE];
	struct sockaddr_in server_addr; 
	int msg_len, bytes_sent, bytes_received;
	
	//tao socket
	client_sock = socket(AF_INET,SOCK_STREAM,0);
	
	//thiet lap thong so
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(serv_port);
	server_addr.sin_addr.s_addr = inet_addr(serv_ip);
	
	//ket noi voi server
	if(connect(client_sock, (struct sockaddr*)&server_addr, sizeof(struct sockaddr)) < 0){
		printf("\nError!Can not connect to sever! Client exit imediately!\n");
		return 0;
	}

	int c;
	int flag = 0;
	char file_path[256];
	
	while (1) {
		printf("\n\n\----------MENU----------\n\n");
		printf("1.Encode\n");
		printf("2.Decode\n");
		printf("3.Thoat\n\n");
		printf("Lua chon: ");
		scanf("%d",&c);
		switch(c) {
		case 1:
			printf("\nDuong dan:");
			memset(file_path,'\0',(strlen(file_path)+1));
			scanf("%s",file_path);
			memset(buff,'\0',(strlen(buff)+1));
			strcpy(buff,"encode");
			break;
		case 2:
			printf("\nDuong dan:");
			memset(file_path,'\0',(strlen(file_path)+1));
			scanf("%s",file_path);
			memset(buff,'\0',(strlen(buff)+1));
			strcpy(buff, "decode");
			break;
		case 3:
			close(client_sock);
			return 0;
		default: printf("Chon lai!!!\n");
		}
	
		//gui yeu cau ma hoa hay giai ma cho server
		bytes_sent = send(client_sock, buff, strlen(buff), 0);
		if(bytes_sent <= 0){
			printf("a\n");
			printf("\nConnection closed!\n");
			return -1;
		}

		//nhan thong bao ok khong tu server
		bytes_received = recv(client_sock, buff, BUFF_SIZE-1, 0);
		if(bytes_received <= 0){
			printf("\nError!Cannot receive data from sever!\n");
			return -1;
		}
		buff[bytes_received] = '\0';
		//printf("Reply from server: %s\n", buff);

		if(strcmp(buff, "OK") != 0)
			return -1;
		
		//guifile
		int fd;
		struct stat stat_buf;
		off_t offset = 0;	
		fd = open(file_path, O_RDONLY);
		if (fd < 0) {
			printf ("unable to open file\n");
		}
		fstat (fd, &stat_buf);
		if ((sendfile(client_sock, fd, &offset, stat_buf.st_size)) == -1) {
			printf("error from sendfile \n");
			exit(EXIT_FAILURE);
		}
		//printf("Gui file xong\n");

		close(fd);
		//shutdown(client_sock,SHUT_WR);
		//nhan file
		FILE *f;
		if ((f = fopen("cli_recv.txt", "w")) == NULL) {
			perror("error creating file");
			return -1;
		}
		int len;
		char buffer[20000];
		len = recv(client_sock, buffer, 20000, 0);
		fwrite(buffer, 1, len, f);
		fclose(f);
		printf("Result in cli_recv.txt\n");
	
	}
	
	//dong cong
	close(client_sock);
	return 0;
}
