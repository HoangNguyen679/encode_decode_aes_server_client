#include <stdio.h>    
#include <stdlib.h>
#include <errno.h>    
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <netdb.h>

#include "lib_code_tmp.h"

#define BACKLOG 10  // so ket noi co the
#define BUFF_SIZE 1024

void sig_chld(int signo);

int main(int argc, char* argv[])
{
	//kiem tra tham so
	int port = 0;
	if (argc == 2) {
			port = atoi(argv[1]);
		if(port < 0)
			return -1;
	} else {
		printf("Sai cu phap: ./server port\n");
		exit(0);
	}

	
	int listen_sock, conn_sock; 
	char recv_data[BUFF_SIZE];
	int bytes_sent, bytes_received;
	struct sockaddr_in server;
	struct sockaddr_in client;
	int sin_size;
	pid_t pid;
	
	//tao socket va lang nghe
	if ((listen_sock = socket(AF_INET, SOCK_STREAM, 0)) == -1 ){  
		perror("\nError: ");
		return 0;
	}
	
	//thuc hien bind va thiet dat co ban
	bzero(&server, sizeof(server));
	server.sin_family = AF_INET;         
	server.sin_port = htons(port); 
	server.sin_addr.s_addr = htonl(INADDR_ANY);  
	if(bind(listen_sock, (struct sockaddr*)&server, sizeof(server))==-1){
		perror("\nError:");
		return 0;
	}     
	
	//lang nghe cac client
	if(listen(listen_sock, BACKLOG) == -1){ 
		perror("\nError: ");
		return 0;
	}
	
	//xu ly signal child
	signal(SIGCHLD, sig_chld);
	
	//Step 4: Communicate with client
	while(1){
		//accept request
		sin_size = sizeof(struct sockaddr_in);
		if ((conn_sock = accept(listen_sock,( struct sockaddr *)&client, &sin_size)) == -1) 
			perror("\nError: ");
  
		printf("You got a connection from %s\n", inet_ntoa(client.sin_addr) ); 
		
		if ((pid = fork()) == 0) {
		 	close(listen_sock);

		 	while (1) {
		 		bytes_received = recv(conn_sock, recv_data, BUFF_SIZE-1, 0);
				if (bytes_received <= 0){
					printf("\nConnection closed\n");
					break;
				}
				else{
					recv_data[bytes_received] = '\0';
					printf("\nReceive: %s\n", recv_data);
				}

				//echo to client
				char *buff_ = "OK";
				bytes_sent = send(conn_sock, buff_, strlen(buff_), 0);
				if (bytes_sent <= 0){
					printf("\nConnection closed\n");
					break;
				}

				FILE* f;
				if ((f = fopen("recv.txt", "w")) == NULL) {
					perror("error creating file");
					return -1;
				}
				int len;
				char buffer[20000];
				memset(buffer, 0, strlen(buffer) + 1);
				len = recv(conn_sock, buffer, 20000, 0);
				fwrite(buffer, 1, len, f);
				fclose(f);
			
				//xu ly
				if(strcmp(recv_data, "encode") == 0)
					encode("recv.txt");
				else if(strcmp(recv_data, "decode") == 0)
					decode("recv.txt");
			
				// gui file tra loi client
				int fd = open("result.txt", O_RDONLY);
				struct stat stat_buf;
				off_t offset = 0;
				if (fd < 0) {
					printf ("unable to open file\n");
				}
				fstat(fd, &stat_buf);
				if ((sendfile(conn_sock, fd, &offset, stat_buf.st_size)) == -1) {
					printf("error from sendfile \n");
					exit(1);
				}
				close(fd);
			}
			close(conn_sock);
			exit(0);
		}	
				
		close(conn_sock);	
	}
	
	close(listen_sock);
	return 0;
}

void sig_chld(int signo) {
    pid_t pid;
    int stat;
    while ((pid = waitpid(-1, &stat, WNOHANG)) > 0)
        printf("child %d terminated\n", pid);
    return;
}

