CC=gcc
CFLAGS=-w

LIBS = -lssl -lcrypto
LDFLAGS = 

PROGS = server client

all: ${PROGS}

server: server.c 
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LIBS)

client: client.c 
	$(CC) $(CFLAGS) -o $@ $^ 

clean: *
	rm -f ${PROGS}
