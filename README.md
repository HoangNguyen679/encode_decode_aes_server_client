# Tranfer - encode file through socket

Encode or decode file by AES (Advanced Encryption Standard)

## Setup

Compile:
```
	make
```

Remove:
```
	make clean
```

## Running
```
	server port_number
	client 127.0.0.1 port_number
```

## Enjoy!
	

